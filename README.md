# Monadic Madness

Give me a monad, and I will tell you who you are.

[![(c) William RM Bartlett, Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

## License

See [LICENSE.md](LICENSE.md)

## Build

Requirements:

- A shell (ash, bash, dash, fish, ksh, ... or zsh)
- Docker 18.09.3
- Haskell Stack 1.9.3

On your favorite shell, run the following command.

```shell
stack build
```

The test suite can be run with the command `stack test`.

If all goes well, a subdirectory named `.stack-work` will appear with a whole mess
of subdirectories, one of which contains the binary.

## Run

The easiest way to run the binary is by using the following command.

```shell
stack run
```

A message will tell which port the service is listening to.

## Use

The service has a few different endpoints

### Endpoint: Hello

The endpoint at http://localhost:8000 should simply reply "hello world".

### Endpoint: Change

The endpoint at http://localhost:8000/change-for/<amount> replies with a way to
give change for the given amount in denominations of 50, 20, 10, 5, 2, 1.

```shell
$ curl -s http://localhost:8000/change-for/88
Just [(50, 1), (20, 1), (10, 1), (5, 1), (2, 1), (1, 1)]
```
