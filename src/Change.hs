module Change (solve) where

import Control.Monad

head_plus_one :: [(Int, Int)] -> [(Int, Int)]
head_plus_one [] = []
head_plus_one ((d, n):dns) = (d, n + 1):dns

solve :: (MonadPlus m) => Int -> [Int] -> m [(Int, Int)]
solve 0 ds = return $ zip ds (repeat 0)
solve n (d:ds) | n > 0 =
  fmap head_plus_one (solve (n - d) (d:ds))
  `mplus` fmap ((d, 0):) (solve n ds)
solve _ _ = mzero
