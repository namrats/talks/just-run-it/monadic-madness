module SubsetSumSpec (spec) where

import Test.Hspec
import SubsetSum

spec :: Spec
spec =
  describe "solve" $ do
    it "returns 1 given 1 and [1]" $ do
      solve 1 [1] `shouldBe` Just [1]
      solve 1 [1] `shouldBe` [[1]]
    it "returns Nothing when no solution" $ do
      solve (-1) [1, 2, 3] `shouldBe` Nothing
      solve (-1) [1, 2, 3] `shouldBe` []
    it "returns 1,2 given 3 and [1,2]" $ do
      solve 3 [1, 2] `shouldBe` Just [1, 2]
      solve 3 [1, 2] `shouldBe` [[1, 2]]
    it "returns 1,3 given 4 and [1,2,3]" $ do
      solve 4 [1, 2, 3] `shouldBe` Just [1, 3]
      solve 4 [1, 2, 3] `shouldBe` [[1, 3]]
    it "negative numbers work" $ do
      solve 4 [-2, 1, 6, -4] `shouldBe` Just [-2, 6]
      solve 4 [-2, 1, 6, -4] `shouldBe` [[-2, 6]]
    it "returns multiple solutions" $ do
      solve 6 [1, 2, 3, 4] `shouldBe` Just [1, 2, 3]
      solve 6 [1, 2, 3, 4] `shouldBe` [[1, 2, 3], [2, 4]]
