module SubsetSum (solve) where

import Control.Monad

solve :: (MonadPlus m) => Int -> [Int] -> m [Int]
solve 0 _ = return []
solve _ [] = mzero
solve n (x:xs) =
  fmap (x:) (solve (n - x) xs)
  `mplus` solve n xs
