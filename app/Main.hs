{-# LANGUAGE OverloadedStrings #-}
module Main where

import Control.Applicative
import Snap.Core
import Snap.Util.FileServe
import Snap.Http.Server
import Data.Maybe (fromMaybe)
import Data.ByteString (ByteString)
import Data.ByteString.Char8 as B8 (pack, unpack)

import qualified Change

main :: IO ()
main = quickHttpServe site

site :: Snap ()
site =
    ifTop (writeBS "hello world") <|>
    route [ ("foo", writeBS "bar")
          , ("echo/:echoparam", echoHandler)
          , ("change-for/:amount", changeHandler)
          ] <|>
    dir "static" (serveDirectory ".")

echoHandler :: Snap ()
echoHandler = do
    param <- getParam "echoparam"
    maybe (writeBS "must specify echo/param in URL")
          writeBS param

getChange :: ByteString -> ByteString
getChange amountBS =
  let amount = read (B8.unpack amountBS) :: Int in
  let result = Change.solve amount [50, 20, 10, 5, 2, 1] :: Maybe [(Int, Int)] in
  B8.pack (show result)

changeHandler :: Snap ()
changeHandler = do
  amount <- getParam "amount"
  writeBS $ getChange (fromMaybe ("0" :: ByteString) amount)
