module ChangeSpec (spec) where

import Test.Hspec
import Change

spec :: Spec
spec =
  describe "change" $ do
    it "given 1 [1], returns [1]" $ do
      solve 1 [1] `shouldBe` Just [(1, 1)]
      solve 1 [1] `shouldBe` [[(1, 1)]]
    it "given 2 [1], returns [2]" $ do
      solve 2 [1] `shouldBe` Just [(1, 2)]
      solve 2 [1] `shouldBe` [[(1, 2)]]
    it "given 5 [5, 1], returns [1, 0]" $ do
      solve 5 [5, 1] `shouldBe` Just [(5, 1), (1, 0)]
      solve 5 [5, 1] `shouldBe` [[(5, 1), (1, 0)], [(5, 0), (1, 5)]]
    it "given 3 [5, 1], returns [0, 3]" $ do
      solve 3 [5, 1] `shouldBe` Just [(5, 0), (1, 3)]
      solve 3 [5, 1] `shouldBe` [[(5, 0), (1, 3)]]
    it "given 6 [5, 1], returns [1, 1]" $ do
      solve 6 [5, 1] `shouldBe` Just [(5, 1), (1, 1)]
